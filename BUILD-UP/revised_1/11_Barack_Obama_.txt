
trees:
(TOP
  (S
    (NP (NNP Barack) (NNP Obama))
    (VP
      (VBD became)
      (NP
        (NP (DT the) (JJ first) (JJ African-American) (NN president))
        (PP (IN of) (NP (DT the) (NNP United) (NNPS States))))
      (PP (IN in) (NP (CD 2009))))
    (. .)))


[Barack Obama/ became the first African-American president/ of the United States/ in 2009.]
    TAG
        SENTECE
    형태
        MC
    역할
        평서문
    뜻
        Barack Obama는 2009년에 미국 최초의 아프리카계 미국인 대통령이 되었다.
    자식
        Barack Obama became the first African-American president of the United States in 2009
            TAG
                MC
            형태
                2형식 (S + Vi + C) / NP + VP + NP + ADJP + ADVP
            역할
                주절
            뜻
                Barack Obama는 2009년에 미국 최초의 아프리카계 미국인 대통령이 되었다
            자식
                Barack Obama
                    TAG
                        NP
                    형태
                        NNP
                    역할
                        주어
                    뜻
                        Barack Obama는
                    자식
                        Barack Obama
                            TAG
                                NNP
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                Barack Obama
                            자식
                                NULL
                became
                    TAG
                        VP
                    형태
                        VBD
                    역할
                        서술어 | 자동사
                    뜻
                        되었다
                    자식
                        became
                            TAG
                                VBD
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                되었다
                            자식
                                NULL
                the first African-American president 
                    TAG
                        NP
                    형태
                        DT + ADJP + NN
                    역할
                        주어 Barack Obama의 주격보어
                    뜻
                        최초의 아프리카계 미국인인 대통령
                    자식
                        the
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                명사 president를 수식하는 정관사
                            뜻
                                그
                            자식
                                NULL
                        first African-American
                            TAG
                                ADJP
                            형태
                                ADJ + ADJ
                            역할
                                명사 president를 수식하는 형용사구
                            뜻
                                최초의 아프리카계 미국인인
                            자식
                                first
                                    TAG
                                        ADJ
                                    형태
                                        NULL
                                    역할
                                        명사 president를 수식하는 형용사
                                    뜻
                                        최초의
                                    자식
                                        NULL
                                African-American
                                    TAG
                                        ADJ
                                    형태
                                        NULL
                                    역할
                                        명사 president를 수식하는 형용사
                                    뜻
                                        아프리카계 미국인인
                                    자식
                                        NULL                                
                        president
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                대통령
                            자식
                                NULL
                of the United States
                    TAG
                        ADJP
                    형태
                        IN + NP
                    역할
                        명사 president 수식하는 형용사구
                    뜻
                        미국의
                    자식
                        of
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끄는 전치사
                            뜻
                                ~의
                            자식
                                NULL
                        the United States
                            TAG
                                NP
                            형태
                                DT + NNPS
                            역할
                                전치사 of의 목적어
                            뜻
                                미국
                            자식
                                the
                                    TAG
                                        DT
                                    형태
                                        NULL
                                    역할
                                        명사 United States 수식하는 정관사
                                    뜻
                                        그
                                    자식
                                        NULL
                                United States
                                    TAG
                                        NNPS
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        미국
                                    자식
                                        NULL                       
                in 2009
                    TAG
                        ADVP
                    형태
                        IN + CD #CD
                    역할
                        동사 became 수식하는 부사구
                    뜻
                        2009년에
                    자식
                        in
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끄는 전치사
                            뜻
                                ~에
                            자식
                                NULL
                        2009
                            TAG
                                CD
                            형태
                                NULL
                            역할
                                전치사 in의 목적어
                            뜻
                                2009
                            자식
                                NULL
        .