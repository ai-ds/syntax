# by seongjun.lim at 2021.02.18

trees:
(TOP
  (S
    (VP
      (VB Think)
      (PP
        (IN about)
        (SBAR
          (WHNP (WP what))
          (S
            (NP (PRP you))
            (VP (MD should) (VP (VB do) (ADVP (RB first))))))))
    (. .)))


[Think about/ what you/ should do/ first.]
    TAG
        SENTENCE
    형태
        MC + SBAR
    역할
        명령문
    뜻
        네가 먼저 해야 하는 것에 대해 생각해라.
    자식
        Think about what you should do first
            TAG
                MC
            형태
                VP + IN + SBAR
            역할
                주절
            뜻
                네가 먼저 해야하는 것에 대해 생각해라
            자식
                Think
                    TAG
                        VP
                    형태
                        VB
                    역할
                        서술어
                    뜻
                        생각해라
                    자식
                        Think
                            TAG
                                VB
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                생각해라
                            자식
                                NULL
                about what you should do first
                    TAG
                        PP
                    형태
                        IN + SBAR
                    역할
                        전명구
                    뜻
                        네가 먼저 해야하는 것에 대해서
                    자식
                        about
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~에 대해서
                            자식
                                NULL
                        what you should do first
                            TAG
                                SBAR
                            형태
                                WHNP + NP + VP + ADVP
                            역할
                                전치사 about의 목적어
                            뜻
                                네가 먼저 해야하는 것
                            자식
                                what
                                    TAG
                                        WHNP
                                    형태
                                        WP
                                    역할
                                        SBAR을 이끈다
                                    뜻
                                        ~ 것
                                    자식
                                        what
                                            TAG
                                                WP
                                            형태
                                                NULL
                                            역할
                                                HEAD
                                            뜻
                                                ~ 것
                                            자식
                                                NULL
                                you
                                    TAG
                                        NP
                                    형태
                                        PRP
                                    역할
                                        SBAR의 주어
                                    뜻
                                        너는
                                    자식
                                        you
                                            TAG
                                                PRP
                                            형태
                                                NULL
                                            역할
                                                HEAD
                                            뜻
                                                너
                                            자식
                                                NULL
                                shoul do
                                    TAG
                                        VP
                                    형태
                                        MD + VB
                                    역할
                                        SBAR의 서술어
                                    뜻
                                        ~해야만한다
                                    자식
                                        should
                                            TAG
                                                MD
                                            형태
                                                NULL
                                            역할
                                                HEAD
                                            뜻
                                                ~해야만하다
                                            자식
                                                NULL
                                        do
                                            TAG
                                                VB
                                            형태
                                                NULL
                                            역할
                                                HEAD
                                            뜻
                                                하다
                                            자식
                                                NULL
                                first
                                    TAG
                                        ADVP
                                    형태
                                        RB
                                    역할
                                        동사 do 수식
                                    뜻
                                        먼저
                                    자식
                                        first
                                            TAG
                                                RB
                                            형태
                                                NULL
                                            역할
                                                HEAD
                                            뜻
                                                먼저
                                            자식
                                                NULL
        .                                
