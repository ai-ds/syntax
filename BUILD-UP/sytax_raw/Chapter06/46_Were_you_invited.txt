# by seongjun.lim at 2021.02.05

trees:
(TOP
  (SQ
    (NNP Were)
    (NP (PRP you))
    (VP
      (VBN invited)
      (PP (TO to) (NP (DT the) (NN fashion) (NN show))))
    (. ?)))


[Were you invited to the fashion show?]
    TAG
        SENTENCE
    형태
        MC
    역할
        의문문
    뜻
        너는 패션쇼에 초대 받았니 ?
    자식
        Were you invited to the fashion show?|
            TAG
                MC
            형태
                1형식 (S + Vi)
            역할
                주절
            뜻
                너는 패션쇼에 초대 받았니 ?
            자식
                Were invited
                    TAG
                        VP
                    형태
                        VBD + VBN
                    역할
                        서술어
                    뜻
                        초대 받다
                    자식
                        Were
                            TAG
                                VBD
                            형태
                                NULL
                            역할
                                수동태를 이끄는 조동사
                            뜻
                                ~ 상태 이었다
                            자식
                                NULL
                        invited
                            TAG
                                VBN
                            형태
                                NULL
                            역할
                                수동태의 분사 역할 | 동사의 원래 뜻을 내포
                            뜻
                                초대받다
                            자식
                                NULL
                you
                    TAG
                        NP
                    형태
                        PRP
                    역할
                        주어
                    뜻
                        너는
                    자식
                        you
                            TAG
                                PRP
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                너
                            자식
                                NULL
                to the fashion show
                    TAG
                        PP
                    형태
                        IN + DT + NN + NN
                    역할
                        invited 수식
                    뜻
                        패션쇼에
                    자식
                        to
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~ 에
                            자식
                                NULL
                        the fashion show
                            TAG
                                NP
                            형태
                                DT + NN
                            역할
                                전치사 to의 목적어
                            뜻
                                그 패션쇼
                            자식
                                the
                                    TAG
                                        DT
                                    형태
                                        NULL
                                    역할
                                        명사 fahshion show 수식
                                    뜻
                                        그
                                    자식
                                        NULL
                                fashion show
                                    TAG
                                        NN
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        패션쇼
                                    자식
                                        NULL
                ?
                    
                