# by seongjun.lim at 2021.02.08

trees:
(TOP
  (S
    (NP (PRP She))
    (VP
      (VBD was)
      (RB n't)
      (VP
        (VBN allowed)
        (S
          (VP
            (TO to)
            (VP
              (VB go)
              (PRT (RP out))
              (ADVP (ADVP (RB late)) (PP (IN at) (NP (NN night))))
              (PP (IN by) (NP (PRP$ her) (NN father))))))))
    (. .)))


[She/ wasn't allowed/ to go out/ late at night/ by her father.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        그녀는 그녀의 아버지에 의해 밤늦게 나가도록 허락되지 않았다.
    자식
        She wasn't allowed to go out late at night by her father
            TAG
                MC
            형태
                2형식 (S + Vi + C)
            역할
                주절
            뜻
                그녀는 그녀의 아버지에 의해 밤늦게 나가도록 허락되지 않았다.
            자식
                She
                    TAG
                        NP
                    형태
                        PRP
                    역할
                        주어
                    뜻
                        그녀는
                    자식
                        She
                            TAG
                                PRP
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                그녀
                            자식
                                NULL
                wasn't allowed
                    TAG
                        VP
                    형태
                        VBD + RB + VB
                    역할
                        서술어
                    뜻
                        허락되지 않았다
                    자식
                        was
                            TAG
                                VBD
                            형태
                                NULL
                            역할
                                수동태를 이끄는 조동사
                            뜻
                                ~ 상태 이었다
                            자식
                                NULL
                        n't
                            TAG
                                RB
                            형태
                                NULL
                            역할
                                조동사 was를 수식하는 부사
                            뜻
                                ~ 않다
                            자식
                                NULL
                        allowed
                            TAG
                                VBN
                            형태
                                NULL
                            역할
                                수동태의 분사 역할 | 동사의 원래 뜻을 내포
                            뜻
                                허락받다
                            자식
                                NULL
                to go out
                    TAG
                        NP
                    형태
                        TO + VP
                    역할
                        주격보어
                    뜻
                        밖으로 나가도록
                    자식
                        to go
                            TAG
                                VP
                            형태
                                TO + VB
                            역할
                                to-infinitive
                            뜻
                                나가도록
                            자식
                                to
                                    TAG
                                        TO
                                    형태
                                        NULL
                                    역할
                                        marker
                                    뜻
                                        ~하도록
                                    자식
                                        NULL
                                go
                                    TAG
                                        VB
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        가다
                                    자식
                                        NULL
                        out
                            TAG
                                RB
                            형태
                                NULL
                            역할
                                동사 go 수식
                            뜻
                                밖으로
                            자식
                                NULL
                late at night
                    TAG
                        ADVP
                    형태
                        RB + IN + NN
                    역할
                        동사 go out을 수식하는 시간부사구
                    뜻
                        밤 늦게
                    자식
                        late
                            TAG
                                ADVP
                            형태
                                RB
                            역할
                                동사 go out 수식
                            뜻
                                늦게
                            자식
                                NULL
                        at night
                            TAG
                                PP
                            형태
                                IN + NN
                            역할
                                동사 go out 수식
                            뜻
                                밤에
                            자식
                                at
                                    TAG
                                        IN
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        ~에
                                    자식
                                        NULL
                                night
                                    TAG
                                        NN
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        밤
                                    자식
                                        NULL
                by her father
                    TAG
                        PP
                    형태
                        IN + NP
                    역할
                        동사 allowed 수식
                    뜻
                        그녀의 아빠에 의해
                    자식
                        by
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~에 의해
                            자식
                                NULL
                        her father
                            TAG
                                NP
                            형태
                                PRP$ + NN
                            역할
                                전치사 by의 목적어
                            뜻
                                그녀의 아빠
                            자식
                                her
                                    TAG
                                        PRP$
                                    형태
                                        NULL
                                    역할
                                        명사 father 수식
                                    뜻
                                        그녀의
                                    자식
                                        NULL
                                father
                                    TAG
                                        NN
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        아빠
                                    자식
                                        NULL
                .
