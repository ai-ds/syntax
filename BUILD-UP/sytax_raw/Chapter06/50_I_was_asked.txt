# by seongjun.lim at 2021.02.05

trees:
(TOP
  (S
    (NP (PRP I))
    (VP
      (VBD was)
      (VP
        (VBN asked)
        (NP (DT some) (JJ difficult) (NNS questions))
        (PP (IN at) (NP (DT the) (NN job) (NN interview)))))
    (. .)))


[I/ was asked/ some difficult questions/ at the job interview.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        나는 면접에서 어려운 질문 몇 개를 받았다.
    자식
        I was asked some difficult questions at the job interview
            TAG
                MC
            형태
                3형식 (S + Vt + O)
            역할
                주절
            뜻
                나는 면접에서 어려운 질문 몇 개를 받았다.
            자식
                I
                    TAG
                        NP
                    형태
                        PRP
                    역할
                        주어
                    뜻
                        나는
                    자식
                        I
                            TAG
                                PRP
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                나
                            자식
                                NULL
                was asked
                    TAG
                        VP
                    형태
                        VBD + VBN
                    역할
                        서술어
                    뜻
                        (질문)받다
                    자식
                        was
                            TAG
                                VBD
                            형태
                                NULL
                            역할
                                수동태를 이끄는 조동사
                            뜻
                                ~ 상태 이었다
                            자식
                                NULL
                        asked
                            TAG
                                VBN
                            형태
                                NULL
                            역할
                                수동태의 분사 역할 | 동사의 원래 뜻을 내포
                            뜻
                                받다
                            자식
                                NULL
                some difficult questions
                    TAG
                        NP
                    형태
                        ADJ + ADJ + NNS
                    역할
                        목적어
                    뜻
                        몇 개의 어려운 질문들
                    자식
                        some
                            TAG
                                ADJ
                            형태
                                NULL
                            역할
                                명사 questions 수식
                            뜻
                                몇 개의
                            자식
                                NULL
                        difficult
                            TAG
                                ADJ
                            형태
                                NULL
                            역할
                                명사 questions 수식
                            뜻
                                어려운
                            자식
                                NULL
                        questions
                            TAG
                                NNS
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                질문들
                            자식
                                NULL
                at the job interview
                    TAG
                        PP
                    형태
                        IN + DT + NN
                    역할
                        asked 수식
                    뜻
                        면접에서
                    자식
                        at
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~ 에서
                            자식
                                NULL
                        the
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                명사 job interview 수식
                            뜻
                                그
                            자식
                                NULL
                        job interview
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                면접
                            자식
                                NULL
                .