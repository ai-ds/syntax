# by heejune.lim at 2021.02.15
paying attention to ~ 부분에 대한 논의 필요

trees:
(TOP
  (FRAG
    (NP (NP (DT Each)) (PP (IN of) (NP (PRP$ her) (NNS friends))))
    (VP
      (VBD was)
      (ADJP
        (ADJP (JJ busy))
        (NP
          (NP (VBG paying))
          (ADVP
            (NP (NN attention))
            (PP (TO to) (NP (PRP themselves)))))))
    (. .)))

[Each of her friends/ was busy (in) paying attention to themselves.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        그녀의 친구들 각각은 그들 스스로에게 집중하느라 바빴다.
    자식
        Each of her friends was busy paying attention to themselves.
            TAG
                MC
            형태
                2형식(S + V + C)
            역할
                주절
            뜻
                그녀의 친구들 각각은 그들 스스로에게 집중하느라 바빴다.
            자식
                Each of her friends
                    Each
                        TAG
                            NN
                        형태
                            NULL
                        역할
                            HEAD
                        뜻
                            각각
                        자식
                            NULL
                    of her friends
                        TAG
                            PP
                        형태
                            IN + PRP$ + NN
                        역할
                            명사 each를 후치수식
                        뜻
                            그녀의 친구들의
                        자식
                            of
                                TAG
                                    IN
                                형태
                                    NULL
                                역할
                                    전치사의 목적어를 이끔
                                뜻
                                    ~의
                                자식
                                    NULL
                            her
                                TAG
                                    PRP$
                                형태
                                    NULL
                                역할
                                    명사 friends 수식
                                뜻
                                    그녀의
                                자식
                                    NULL
                            friends
                                TAG
                                    NN
                                형태
                                    NULL
                                역할
                                    HEAD
                                뜻
                                    친구들
                                자식
                                    NULL
                was 
                    TAG
                        VP
                    형태
                        VBD + ADJ
                    역할
                        서술어
                    뜻
                        그들 자신에게 집중하느라 바빴다
                    자식
                        was
                            TAG
                                VBD
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                이었다
                            자식
                                NULL
                busy
                    TAG
                        ADJP
                    형태
                        ADJ
                    역할
                        보어
                    뜻
                        바쁜
                    자녀
                        busy
                            TAG
                                ADJ
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                바쁜
                            자녀
                                NULL
                paying attention to themselves
                    TAG
                        ADJP
                    형태
                        VBG + NN + IN + PRP
                    역할
                        전명구(부사구)
                    뜻
                        그들 스스로에게 집중하느라 바쁜
                    자녀
                        paying
                            TAG
                                VBG
                            형태
                                NULL
                            역할
                                전치사 in의 목적어
                            뜻

                            자녀
                        attention
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                동사구 pay attention to 를 구성
                            뜻
                                ~에 집중하다
                            자녀
                                NULL
                        to
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                목적어 themselves를 이끔
                            뜻
                                ~에
                            자녀
                                NULL
                        themselves
                            TAG
                                PRP
                            형태
                                NULL
                            역할
                                전치사 to의 목적어
                            뜻
                                그들 스스로
                            자녀
                                NULL
        .
