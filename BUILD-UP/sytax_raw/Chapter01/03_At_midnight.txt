# by seongjun.lim at 2021.02.02

trees:
(TOP
  (S
    (PP (IN At) (NP (NN midnight)))
    (, ,)
    (NP (DT the) (NN temperature))
    (VP (VBZ drops) (PP (IN below) (NP (NN zero))))
    (. .)))

[At midnight,/ the temperature/ drops/ below zero.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        자정에, 기온은 영하로 떨어진다.           
    자식
        At midnight, the temperature drops below zero
            TAG
                MC
            형태
                1형식 (S + Vi) # ADVP + NP + VP + ADVP
            역할
                평서문
            뜻
                자정에는, 기온이 영하로 떨어진다.
            자식
                At midnight
                    TAG
                        PP
                    형태
                        IN + NN
                    역할
                        문장 전체 수식하는 부사구
                    뜻
                        자정에는
                    자식
                        At
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~에
                            자식
                                NULL
                        midnight
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                전치사 at의 목적어
                            뜻
                                자정
                            자식
                                NULL
                ,
                the temperature
                    TAG
                        NP
                    형태
                        DT + NN
                    역할
                        주어
                    뜻
                        기온은
                    자식
                        the
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                명사 temperature 수식
                            뜻
                                그
                            자식
                                NULL
                        temperature
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                온도
                            자식
                                NULL               
                drops
                    TAG
                        VP
                    형태
                        VBZ
                    역할
                        서술어 | 자동사
                    뜻
                        떨어지다
                    자식
                        drops
                            TAG 
                                VBZ
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                떨어지다
                            자식
                                NULL
                below zero
                    TAG
                        PP
                    형태
                        IN + NN
                    역할
                        동사 drops를 수식하는 부사구
                    뜻
                        영하로
                    자식
                        below
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                아래로
                            자식
                                NULL
                        zero
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                전치사 below의 목적어
                            뜻
                                영
                            자식
                                NULL
        .
                