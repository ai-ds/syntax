# by seongjun.lim at 2021.02.10

trees:
(TOP
  (S
    (NP (PRP I))
    (VP
      (VBD got)
      (S (NP (PRP$ my) (NN arm)) (ADJP (NN broken)))
      (NP (NN yesterday)))
    (. .)))


[I/ got/ my arm/ broken/ yesterday.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        나는 어제 팔이 부러졌다.
    자식
        I got my arm broken yesterday.
            TAG
                MC
            형태
                5형식 (S + Vt + O + OC)
            역할
                주절
            뜻
                나는 어제 팔이 부러졌다.
            자식
                I
                    TAG
                        NP
                    형태
                        PRP
                    역할
                        주어
                    뜻
                        나는
                    자식
                        I
                            TAG
                                PRP
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                나
                            자식    
                                NULL
                got
                    TAG
                        VP
                    형태
                        VBD
                    역할
                        서술어 | 사역동사
                    뜻
                        O가 OC를 당하다 (S의 의지,의도가 아닐 경우)
                    자식
                        got
                            TAG
                                VBD
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                O가 OC를 당하다
                            자식
                                NULL
                my arm
                    TAG
                        NP
                    형태
                        PRP$ + NN
                    역할
                        목적어
                    뜻
                        나의 팔
                    자식
                        my
                            TAG
                                PRP$
                            형태
                                NULL
                            역할
                                명사 arm 수식
                            뜻
                                나의
                            자식
                                NULL
                        arm
                            TAG
                                NN
                            형태
                                NULL
                            역할

                            뜻
                            자식
                                NULL
                broken
                    TAG
                        VP
                    형태
                        VBN
                    역할
                        목적보어
                    뜻
                        부러지다
                    자식
                        broken
                            TAG
                                VBN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                부러지다
                            자식
                                NULL
                yesterday
                    TAG
                        ADVP
                    형태
                        RB
                    역할
                        시간의 부사구
                    뜻
                       어제 
                    자식
                        yesterday
                            TAG
                                RB
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                어제
                            자식
                                NULL

                .
                