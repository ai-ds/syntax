# by seongjun.lim at 2021.02.04

trees:
(TOP
  (S
    (NP (DT The) (JJ English))
    (VP
      (NN consider)
      (S
        (NP (NP (NNS sports)) (PP (IN in) (NP (NN school))))
        (NP
          (NP (DT an) (JJ important) (NN paryt))
          (PP (IN of) (NP (NN education))))))
    (. .)))


[The English/ consider/ sports/ in school/ an important part/ of education.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        영국인들은 학교에서의 스포츠를 교육의 중요한 부분으로 간주한다.
    자식
        The English consider sports in school an important paryt of education
            TAG
                MC
            형태
                5형식 (S + Vt + O + OC)
            역할
                주절
            뜻
                영국인들은 학교에서의 스포츠를 교육의 중요한 부분으로 간주한다
            자식
                The English
                    TAG
                        NP
                    형태
                        DT + NN
                    역할
                        주어
                    뜻
                        영국인들은
                    자식
                        The
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                명사 English 수식
                            뜻
                                그
                            자식
                                NULL
                        English
                            TAG
                                NNPS
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                영어
                            자식
                                NULL
                consider
                    TAG
                        VP
                    형태
                        VB
                    역할
                        서술어
                    뜻
                        간주한다
                    자식
                        consider
                            TAG
                                VB
                            형태
                                NULL
                            역할
                                타동사
                            뜻
                                간주하다
                            자식
                                NULL
                sports
                    TAG
                        NP
                    형태
                        NNS
                    역할
                        목적어
                    뜻
                        스포츠를
                    자식
                        sports
                            TAG
                                NNS
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                스포츠
                            자식
                                NULL
                in school
                    TAG
                        PP
                    형태
                        IN + NN
                    역할
                        명사 sports를 수식하는 전명구
                    뜻
                        학교에서
                    자식
                        in
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~에서
                            자식
                                NULL
                        school
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                전치사 in의 목적어
                            뜻
                                학교
                            자식
                                NULL
                an important part
                    TAG
                        NP
                    형태
                        DT + ADJ + NN
                    역할
                        목적보어
                    뜻
                        중요한 부분으로
                    자식
                        an
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                명사 part 수식
                            뜻
                                하나, 한
                            자식
                                NULL
                        important
                            TAG
                                ADJ
                            형태
                                NULL
                            역할
                                명사 part 수식
                            뜻
                                중요한
                            자식
                                NULL
                        part
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                부분
                            자식
                                NULL
                of education
                    TAG
                        PP
                    형태
                        IN + NN
                    역할
                        명사 part 수식
                    뜻
                        교육의
                    자식
                        of
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~의
                            자식
                                NULL
                        education
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                전치사 of의 목적어
                            뜻
                                교육
                            자식
                                NULL            
        .