# by seongjun.lim at 2021.02.16

trees:
(TOP
  (SINV
    (ADJP (JJ Impossible))
    (VP (VBZ is))
    (NP
      (NP (DT a) (NN word))
      (SBAR
        (S
          (VP
            (TO to)
            (VP
              (VB be)
              (VP
                (VBN found)
                (PP
                  (IN in)
                  (NP
                    (NP (DT the) (NN dictionary))
                    (PP (IN of) (NP (NNS fools)))))))))))))


[Impossible/ is/ a word to be found/ in the dictionary of fools.]
    TAG
        SENTENCE
    형태
        MC
    역할
        평서문
    뜻
        불가능은 바보들의 사전에서 발견되는 단어이다.
    자식
        Impossible is a word to be found in the dictionary of fools
            TAG
                MC
            형태
                2형식 (S + Vi + C)
            역할
                주절
            뜻
                불가능은 바보들의 사전에서 발견되는 단어이다
            자식
                Impossible
                    TAG
                        NP
                    형태
                        NN
                    역할
                        주어
                    뜻
                        불가능은
                    자식
                        Impossible
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                불가능
                            자식
                                NULL
                is
                    TAG
                        VP
                    형태
                        VBZ
                    역할
                        서술어 | 자동사
                    뜻
                        ~이다
                    자식
                        is
                            TAG
                                VBZ
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                ~이다
                            자식
                                NULL
                a word
                    TAG
                        NP
                    형태
                        DT + NN
                    역할
                        보어
                    뜻
                        발견되는 단어
                    자식
                        a
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                명사 word 수식
                            뜻
                                하나의 ,한 개의
                            자식
                                NULL
                        word
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                단어
                            자식
                                NULL
                (infl)
                to be found
                    TAG
                        VP
                    형태
                        TO + VB + VBN
                    역할
                        to-infinitive # 명사 word 수식
                    뜻
                        발견되는
                    자식
                        to
                            TAG
                                TO
                            형태
                                NULL
                            역할
                                marker
                            뜻
                                ~ 는
                            자식
                                NULL
                        be 
                            TAG
                                VB
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                ~이다
                            자식
                                NULL
                        found
                            TAG
                                VBN
                            형태
                                NULL
                            역할
                                동사 be의 보어
                            뜻
                                발견된
                            자식
                                NULL
                in the dictionary of fools
                    TAG
                        PP
                    형태
                        IN + NP + IN + NNS
                    역할
                        to be found 수식
                    뜻
                        바보들의 사전에서
                    자식
                        in
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전명구를 이끈다
                            뜻
                                ~에서
                            자식
                                NULL
                        the dictionary
                            TAG
                                NP
                            형태
                                DT + NN
                            역할
                                전치사 in의 목적어
                            뜻
                                그 사전
                            자식
                                the
                                    TAG
                                        DT
                                    형태
                                        NULL
                                    역할
                                        명사 dictionary 수식
                                    뜻
                                        그
                                    자식
                                        NULL
                                dictionary
                                    TAG
                                        NN
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        사전
                                    자식
                                        NULL
                        of fools
                            TAG
                                PP
                            형태
                                IN + NNS
                            역할
                                명사 dictionary 수식
                            뜻
                                바보들의
                            자식
                                of
                                    TAG
                                        IN
                                    형태
                                        NULL
                                    역할
                                        전명구를 이끈다
                                    뜻
                                        ~의
                                    자식
                                        NULL
                                fools
                                    TAG
                                        NNS
                                    형태
                                        NULL
                                    역할
                                        전치사 of의 목적어
                                    뜻
                                        바보들
                                    자식
                                        NULL
                .
        