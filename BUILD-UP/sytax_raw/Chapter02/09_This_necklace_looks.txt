# by seongjun.lim at 2021.02.02


trees:
(TOP
  (S
    (NP (DT This) (NN necklace))
    (VP
      (VBZ looks)
      (ADJP (RB too) (JJ tight))
      (PP (IN for) (NP (PRP$ your) (NN neck))))
    (. .)))


[This necklace/ looks too tight/ for your neck.]
    TAG
        SENTENCE
    형태
        2형식 (S + Vi + C)
    역할
        평서문
    뜻
        이 목걸이는 너의 목에 너무 딱 맞게 보인다.
    자식
        This necklace looks too tight for your neck.
            TAG
                MC
            형태
                2형식 (S + Vi + C)
            역할
                평서문
            뜻
                이 목걸이는 너의 목에 너무 딱 맞게 보인다
            자식
                This necklace
                    TAG
                        NP
                    형태
                        DT + NN
                    역할
                        주어
                    뜻
                        이 목걸이는
                    자식
                        This
                            TAG
                                DT
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                이것
                            자식
                                NULL
                        necklace
                            TAG
                                NN
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                목걸이
                            자식
                                NULL
                looks 
                    TAG
                        VP
                    형태
                        VBZ
                    역할
                        서술어 | 자동사
                    뜻
                        보인다
                    자식
                        looks
                            TAG
                                VBZ
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                보인다
                            자식
                                NULL
                too tight # too, tight 따로 
                    TAG
                        ADJP
                    형태
                        ADV + ADJ
                    역할
                        주격보어
                    뜻
                        너무 꽉 낀
                    자식
                        too
                            TAG
                                RB
                            형태
                                NULL
                            역할
                                형용사 tight를 수식하는 부사
                            뜻
                                너무
                            자식
                                NULL
                        tight
                            TAG
                                ADJ
                            형태
                                NULL
                            역할
                                HEAD
                            뜻
                                꽉 끼는
                            자식
                                NULL
                for your neck # tight를 꾸며주는 부사구
                    TAG
                        PP
                    형태
                        IN + NP
                    역할
                        동사 looks 수식
                    뜻
                        너의 목에
                    자식
                        for
                            TAG
                                IN
                            형태
                                NULL
                            역할
                                전치사구를 이끈다
                            뜻
                               ~에
                            자식
                                NULL
                        your neck
                            TAG
                                NP
                            형태
                                PRP$ + NN
                            역할
                                전치사 for의 목적어
                            뜻
                                너의 목
                            자식
                                your
                                    TAG
                                        PRP$
                                    형태
                                        NULL
                                    역할
                                        명사 neck 수식
                                    뜻
                                        너의
                                    자식
                                        NULL
                                neck
                                    TAG
                                        NN
                                    형태
                                        NULL
                                    역할
                                        HEAD
                                    뜻
                                        목
                                    자식
                                        NULL
        .